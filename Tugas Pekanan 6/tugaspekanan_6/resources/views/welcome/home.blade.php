@extends('layout.master')
@section('title')
Halaman Beranda
@endsection
@section('subtitle')
Beranda
@endsection
@section('content')
<h1 style="text-transform: capitalize;">Selamat Datang {{ $namaDepan }} {{ $namaBelakang }}!</h1><br>
<h2>Terima kasih telah bergabung di SanberBook. Social Media kita bersama!</h2>
@endsection
