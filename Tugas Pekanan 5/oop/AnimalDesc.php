<?php

require_once('Animal.php');

class Frog extends Animal
{
    public function jump()
    {
        return "Hop Hop <br><br>"; 
    }
}

class Ape extends Animal
{
    public $legs = 2;
    
    public function yell()
    {
        return "Auooo <br><br>"; 
    }
}

?>