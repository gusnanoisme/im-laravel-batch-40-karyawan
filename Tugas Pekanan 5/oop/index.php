<?php

echo "<h1>1. OOP PHP</h1>";

require_once('Animal.php');
require_once('AnimalDesc.php');

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>"; // false

$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>"; // buduk
echo "Legs : " . $kodok->legs . "<br>"; // 4
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>"; // false
echo "Jump : " . $kodok->jump(); //Hop Hop

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "<br>"; // kera sakti
echo "Legs : " . $sungokong->legs . "<br>"; // 2
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>"; // false
echo "Yell : " . $sungokong->yell(); //Auooo

?>