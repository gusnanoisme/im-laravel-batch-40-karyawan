<?php

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\ReviewController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'index']);
Route::get('/registration', [AuthController::class, 'regist']);
Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/data-table', [IndexController::class, 'table']);

Route::group(['middleware' => ['auth']], function () {
    // CRUD Cast

    // Create Data
    Route::get('/cast/create', [CastController::class, 'create']);

    // Store Data
    Route::post('/cast', [CastController::class, 'store']);

    // Read All Data
    Route::get('/cast', [CastController::class, 'index']);

    // Show Detail Data by ID
    Route::get('/cast/{cast_id}', [CastController::class, 'show']);

    // Update Data
    Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

    // Update Data by ID
    Route::put('/cast/{cast_id}', [CastController::class, 'update']);

    // Delete Data
    Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);


    // CRUD Genre

    // Create Data Genre
    Route::get('/genre/create', [GenreController::class, 'create']);

    // Store Data
    Route::post('/genre', [GenreController::class, 'store']);

    // Read All Data
    Route::get('/genre', [GenreController::class, 'index']);

    // Show Detail Data by ID
    Route::get('/genre/{genre_id}', [GenreController::class, 'show']);

    // Update Data
    Route::get('/genre/{genre_id}/edit', [GenreController::class, 'edit']);

    // Update Data by ID
    Route::put('/genre/{genre_id}', [GenreController::class, 'update']);

    // Delete Data
    Route::delete('/genre/{genre_id}', [GenreController::class, 'destroy']);

    Route::post('/review/{film_id}', [ReviewController::class, 'store']);

    
    });
    //CRUD Film
    Route::resource('film', FilmController::class);


Auth::routes();

