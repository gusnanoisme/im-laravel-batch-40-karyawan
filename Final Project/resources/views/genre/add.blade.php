@extends('layout.master')
@section('title')
Halaman Tambah Genre
@endsection
@section('subtitle')
Data Genre
@endsection
@section('content')
<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
      <label>Genre</label>
      <input type="text" class="form-control" name="genre_name">
      @error('genre_name')
         <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>      
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
