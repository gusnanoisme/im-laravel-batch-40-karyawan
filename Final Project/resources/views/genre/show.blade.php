@extends('layout.master')
@section('title')
Halaman Tampil Genre
@endsection
@section('subtitle')
Data Genre
@endsection
@section('content')
<a href="/genre/create" class="btn btn-primary my-2">Add</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Genre</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genres as $key=>$item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->genre_name}}</td>
                <td>                    
                    <form action="/genre/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/genre/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Data is Empty</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection
