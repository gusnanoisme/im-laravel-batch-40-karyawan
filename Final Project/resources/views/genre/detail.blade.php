@extends('layout.master')
@section('title')
Halaman Detail Data Genre
@endsection
@section('subtitle')
Data Genre
@endsection
@section('content')
<h1>{{$genres->genre_name}}</h1>

<div class="row">
    @forelse ($genres->film as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{asset('images/' . $item->poster)}}" class="card-img-top" width=200px height=400px alt="Image here">
                <div class="card-body">
                    <h4>{{$item->title}}</h4>
                    <p>{{$item->year}}</p>
                    <p class="card-text">{{Str::limit($item->synopsis, 100)}}</p>
                    <a href="/film/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Sinopsis</a>
                </div>
            </div>
        </div>
    @empty
        
    @endforelse
</div>

@endsection