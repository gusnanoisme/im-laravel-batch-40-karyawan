@extends('layout.master')
@section('title')
Halaman Update Data Cast
@endsection
@section('subtitle')
Data Cast
@endsection
@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Cast Name</label>
      <input type="text" class="form-control" value="{{$cast->cast_name}}" name="cast_name">
      @error('cast_name')
         <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label>Cast Age</label>
      <input type="text" class="form-control" value="{{$cast->cast_age}}"name="cast_age">
      @error('cast_age')
         <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div> 
    <div class="form-group">
        <label>Cast Biodata</label>
        <textarea name="cast_bio" class="form-control" cols="30" rows="10">{{$cast->cast_bio}}</textarea>
        @error('cast_bio')
         <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
