@extends('layout.master')
@section('title')
Halaman Detail Data Cast
@endsection
@section('subtitle')
Data Cast
@endsection
@section('content')
<h1>{{$cast->cast_name}}</h1>
<h4>{{$cast->cast_age}} years old</h4>
<p>{{$cast->cast_bio}}</p>
@endsection