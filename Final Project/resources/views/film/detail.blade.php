@extends('layout.master')
@section('title')
Halaman Detail Film
@endsection
@section('subtitle')
Detail Film
@endsection
@section('content')

<div class="row">
    <div class="col">
        <div class="card">
            <img src="{{asset('images/' . $film->poster)}}" class="card-img-top rounded mx-auto d-block w-50 h-50 mb-5" alt="Image here">
            <div class="card-body">
                <h4>{{$film->title}}</h4>
                <p>{{$film->year}}</p>
                <p class="card-text">{{$film->synopsis}}</p>
                <a href="/film" class="btn btn-secondary btn-sm">Back</a>
            </div>            
        </div>
    </div>    
</div>
<hr>

@forelse ($film->review as $item)
    <div class="card">
        <div class="card-header">
            <b>{{$item->user->name}}</b>
        </div>
        <div class="card-body">
            <h4 class="card-title"> 
                @if ($item->point === 1)
                    ★☆☆☆☆ 
                @elseif ($item->point === 2)
                    ★★☆☆☆
                @elseif ($item->point === 3)
                    ★★★☆☆
                @elseif ($item->point === 4)
                    ★★★★☆
                @else
                    ★★★★★
                @endif
            </h4> <br>
            <p class="card-text"><i>{{$item->content}}</i></p>
        </div>
    </div>
@empty
    <h4>No Review</h4>
@endforelse

<hr>
    <div>
        <form action="/review/{{$film->id}}" method="POST">
            @csrf
            <select name="point" class="form-control" id="">
                <option value="">--Rating--</option>
                <option value="1">★</option>
                <option value="2">★★</option>
                <option value="3">★★★</option>
                <option value="4">★★★★</option>
                <option value="5">★★★★★</option>
            </select><br>
            
            <textarea name="content" class="form-control" id="" cols="30" rows="10" placeholder="Isi ulasan"></textarea>
            <input type="submit" class="btn btn-block btn-primary mt-2" value="Kirim">
        </form>
    </div>        
@endsection