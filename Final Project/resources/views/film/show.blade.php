@extends('layout.master')
@section('title')
Halaman Tampil Film
@endsection
@section('subtitle')
List Film
@endsection
@section('content')

@auth
<a href="/film/create" class="btn btn-primary my-2">Add Film</a>
@endauth

@guest
    <div class="row">
    @forelse ($film as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{asset('images/' . $item->poster)}}" class="card-img-top" width=200px height=400px alt="Image here">
                <div class="card-body">
                    <h4>{{$item->title}}</h4>
                    <p>{{$item->year}}</p>
                    <small class="text-info">{{$item->genre->genre_name}}</small>
                    <p class="card-text">{{Str::limit($item->synopsis, 100)}}</p>
                    <a href="/film/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Info Selengkapnya</a>
                    <div class="row my-2">
                        @auth
                        <div class="col">
                            <a href="/film/{{$item->id}}/edit" class="btn btn-info btn-block btn-sm">Edit</a>
                        </div>
                        <div class="col">
                            <form action="/film/{{$item->id}}" method="POST">
                                @csrf
                                @method('delete')
                                <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">
                            </form>
                        </div>
                        @endauth                        
                    </div>
                </div>
            </div>
        </div>        
    @empty
        <h5>No Film</h5>
    @endforelse    
</div>
@endguest

@endsection
