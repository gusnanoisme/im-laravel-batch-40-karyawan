@extends('layout.master')
@section('title')
Halaman Tambah Film
@endsection
@section('subtitle')
Data Film
@endsection
@section('content')
<form action="/film" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Judul Film</label>
      <input type="text" class="form-control" name="title">
      @error('title')
         <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
        <label>Synopsis</label>
        <textarea name="synopsis" class="form-control" cols="30" rows="10"></textarea>
        @error('synopsis')
         <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>    
    <div class="form-group">
      <label>Tahun Rilis</label>
      <input type="text" class="form-control" name="year">
      @error('year')
         <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div> 
    <div class="form-group">
        <label>Poster</label>
        <input type="file" class="form-control" name="poster">
        @error('poster')
           <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div> 
    <div class="form-group">
        <label>Genre</label>
        <select name="genre_id" class="form-control" id="">
            <option value="">--Pilih Genre--</option>
            @forelse ($genres as $item)
                <option value="{{$item->id}}">{{$item->genre_name}}</option>
            @empty
                <option value="">No Genre</option>
            @endforelse
        </select>
        @error('genre_id')
           <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div> 
   
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
