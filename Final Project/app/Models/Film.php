<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;
    protected $table = 'films';
    protected $fillable = [
        'title',
        'synopsis',
        'year',
        'poster',
        'genre_id'
    ];    

    public function genres()
    {
        return $this->belongsTo(Genre::class, 'genre_id');
    }

    public function review()
    {
        return $this->hasMany(Review::class, 'film_id');
    }
}
