<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Review;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    public function store(Request $request, $id)
    {
        $request->validate([
            'content'=> 'required',
            'point'=>'required'
            
        ]);

        Review::create([
            'content' => $request['content'],
            'point' => $request['point'],
            'user_id' => Auth::id(),
            'film_id' => $id
        ]);

        return redirect('/film/' . $id);
    }
}
