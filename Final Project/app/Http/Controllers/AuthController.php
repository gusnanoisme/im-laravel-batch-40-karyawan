<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regist()
    {
        return view('registration.form');
    }

    public function welcome(Request $request)
    {
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('welcome.home', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
