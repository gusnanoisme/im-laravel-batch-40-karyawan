@extends('layout.master')
@section('title')
Halaman Pendaftaran
@endsection
@section('subtitle')
Daftar
@endsection
@section('content')
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action="/welcome" method="POST">
    @csrf
    <label>First name:</label><br>
    <input type="text" name="fname"><br><br>
    <label>Last name:</label><br>
    <input type="text" name="lname"><br><br>

    <label>Gender:</label><br>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female<br>
    <input type="radio" name="gender">Other<br><br>

    <label>Nationality:</label><br>
    <select name="wn">
        <option value="id">Indonesian</option>
        <option value="sgp">Singaporean</option>
        <option value="mal">Malaysian</option>
        <option value="aus">Australian</option>
    </select><br><br>

    <label>Language Spoken:</label><br>
    <input type="checkbox" name="lang1" value="Bahasa Indonesia">Bahasa Indonesia <br>
    <input type="checkbox" name="lang2" value="English">English<br>
    <input type="checkbox" name="lang3" value="Other">Other<br><br>

    <label>Bio:</label><br>
    <textarea name="bio" cols="30" rows="10"></textarea><br>

    <input type="submit" value="Sign Up">

</form>
@endsection
