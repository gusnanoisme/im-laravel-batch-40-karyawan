@extends('layout.master')
@section('title')
Halaman Tambah Data Cast
@endsection
@section('subtitle')
Data Cast
@endsection
@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Cast Name</label>
      <input type="text" class="form-control" name="cast_name">
      @error('cast_name')
         <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label>Cast Age</label>
      <input type="text" class="form-control" name="cast_age">
      @error('cast_age')
         <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div> 
    <div class="form-group">
        <label>Cast Biodata</label>
        <textarea name="cast_bio" class="form-control" cols="30" rows="10"></textarea>
        @error('cast_bio')
         <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
