@extends('layout.master')
@section('title')
Halaman Tampil Data Cast
@endsection
@section('subtitle')
Data Cast
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary my-2">Add</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Cast Name</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($casts as $key=>$item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->cast_name}}</td>
                <td>                    
                    <form action="/cast/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Data is Empty</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection
