<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $casts = Cast::all();

        return view('cast.show', ['casts' => $casts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cast.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cast_name' => 'required',
            'cast_age' => 'required',
            'cast_bio' => 'required',
        ]);

        // Store data to database
        Cast::create([
            'cast_name' => $request['cast_name'],
            'cast_age' => $request['cast_age'],
            'cast_bio' => $request['cast_bio'],
        ]);

        // Redirect to Cast Table Page
        return redirect('/cast');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cast = Cast::find($id);

        return view('cast.detail', ['cast' => $cast]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = Cast::find($id);

        return view('cast.update', ['cast' => $cast]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'cast_name' => 'required',
            'cast_age' => 'required',
            'cast_bio' => 'required',
        ]);

        Cast::where('id', $id)
            ->update([
                'cast_name' => $request['cast_name'],
                'cast_age' => $request['cast_age'],
                'cast_bio' => $request['cast_bio'],
            ]);

        return redirect('/cast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cast = Cast::find($id);
        $cast->delete();
        
        return redirect('/cast');
    }
}
